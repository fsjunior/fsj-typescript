/**
 * Created by fernando.silva on 19/10/2016.
 */

class SystemAlert{

    static alert(message:string):void{
        console.log(message);
    }

    static warm (message:string):void{
        console.log('Atenção: ' + message);
    }

    static error(message:string):void{
        console.log('Erro: ' + message);
    }

}

SystemAlert.alert('Oi');
SystemAlert.warm('Um alerta básico');
SystemAlert.error('Não foi possível conectar na base de dados');