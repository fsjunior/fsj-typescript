/**
 * Created by fernando.silva on 19/10/2016.
 */

enum Color {
    Blue  = 1,
    Green = 2,
    Red   = 3
}

var c:Color = Color.Green;
console.log('Color.Green => ', c);
