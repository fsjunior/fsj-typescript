/**
 * Created by fernando.silva on 19/10/2016.
 */

class Bar {
    buildName(firstName: string, lastName? : string) {
        if (lastName){
            console.log('Tem o último parâmetro...', lastName);
        }

        console.log('Bar.buildName().firstName => ', firstName);
    }
}

var bar = new Bar();
bar.buildName('Fernando', 'Junior');
