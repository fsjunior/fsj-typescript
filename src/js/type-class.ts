/**
 * Created by fernando.silva on 19/10/2016.
 */

/**
 * 
 */
class Greeter {
    private greeting: string;  // seta a visibilidade do atributo para 'private'
    constructor(message: string) {
        this.greeting = message;
    }
    public greet(): string {  // define o tipo de retorno do método como 'string
        return "Hello, " + this.greeting;
    }
}

var greeter = new Greeter("world");
