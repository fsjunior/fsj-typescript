/**
 * Created by fernando.silva on 19/10/2016.
 */

class PessoaGS {
    private _password: string;  // define as a 'string' type

    get password(): string {    // define the return as a 'string' type
        return this._password;
    }

    set password(p : string) {  // define the parameter type as a 'string'
        if (p == '123456') {
            alert('Ei, senha não pode ser 123456');
        }
        else {
            this._password = p;
        }
    }
}

var p = new PessoaGS();
p.password = '123456'; //vai exibir o erro