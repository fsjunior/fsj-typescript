/**
 * Created by fernando.silva on 19/10/2016.
 */

interface Ponto{
    x: number;
    y: number;
    z: number;
}