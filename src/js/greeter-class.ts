/**
 * Created by fernando.silva on 19/10/2016.
 */
class Student {
    fullname : string;
    constructor(public firstname, public middleinitial, public lastname) {
        this.fullname = firstname + ' ' + middleinitial + ' ' + lastname;
    }
}

interface Person {
    firstname: string;
    lastname: string;
}

function greeterUser(person: Person) {
    return 'Hello, ' + person.firstname + ' ' + person.lastname;
}

var user = new Student('Fernando', 'da S.', 'Manolo');

document.body.innerHTML = greeterUser(user);
console.log(greeterUser(user));