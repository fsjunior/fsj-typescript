/**
 * Created by fernando.silva on 19/10/2016.
 */

interface SimplePerson {
    firstname: string;
    lastname: string;
}

function greeterI(person : SimplePerson) {
    return 'Hello, ' + person.firstname + ' ' + person.lastname;
}

var userPerson = {firstname: 'Fernando', lastname: 'Junior'};

document.body.innerHTML = greeterI(userPerson);
console.log(greeterI(userPerson));