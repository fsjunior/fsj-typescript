/**
 * Created by fernando.silva on 19/10/2016.
 */

class Foo2 {
    static logName(firstName: string, ...restOfName: string[]) {
        console.log('Foo2.logName()', firstName + ' ' + restOfName.join(' '));
        console.log('Foo2.logName().restOfName', restOfName);
    }
}
Foo2.logName('Fulano', 'de', 'Tal');