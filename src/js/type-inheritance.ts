/**
 * Created by fernando.silva on 19/10/2016.
 */

class Animal {
    name:string;
    constructor(theName: string) { this.name = theName; }
    move(meters: number = 0) {
        alert(this.name + ' moved ' + meters + 'm.');
    }
}

class Snake extends Animal {
    constructor(name: string) { super(name); }  // 'super' equivale a this.parent de outras linguagens
    move(meters = 5) {
        alert('Slithering...');
        super.move(meters);
    }
}

class Horse extends Animal {
    constructor(name: string) { super(name); }
    move(meters = 45) {
        alert('Galloping...');
        super.move(meters);
    }
}

var sam = new Snake('Sammy the Python');
var tom: Animal = new Horse('Tommy the Palomino');

sam.move();
tom.move(34);
