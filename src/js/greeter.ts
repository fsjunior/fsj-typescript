/**
 * Created by fernando.silva on 10/18/16.
 */

function simpleGreeter(person: string) {
    return 'Hello, ' + person;
}

var userName = 'Fernando Jr';
// userName = [1,2,3];  // Throw a error

document.body.innerHTML = simpleGreeter(userName); // If supplied parameter, throw a error
console.log(simpleGreeter(userName));