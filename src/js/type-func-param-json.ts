/**
 * Created by fernando.silva on 19/10/2016.
 */

class Ponto2{

    private _x:number = 0;
    private _y:number = 0;
    private _z:number = 0;

    constructor( p: {x:number; y:number; z?:number;} ){
        this._x = p.x;
        this._y = p.y;
        if (p.z) this._z = p.z;
    }

    is3d():boolean{
        return this._z != 0;
    }

}

var p2 = new Ponto2({x:10, y:20});
console.log('p2.is3d()', p2.is3d());

var p3 = new Ponto2({x:10, y:20, z: 15});
console.log('p3.is3d()', p3.is3d());
