/**
 * Created by fernando.silva on 19/10/2016.
 */

var notSure : any = 4;
notSure = 'maybe a string instead';
notSure = false; // okay, definitely a boolean

var list:any[] = [1, true, 'free'];
list[1] = 100;
