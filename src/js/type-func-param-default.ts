/**
 * Created by fernando.silva on 19/10/2016.
 */

function buildName(firstName: string, lastName : string = 'Smith') {
    return firstName + ' ' + lastName;
}

// or

class Foo {
    buildName(firstName: string, lastName : string = 'Smith') {
        return firstName + ' ' + lastName;
    }
}

var b = buildName('Fernando'),
    f = new Foo();

console.log('buildName => ', b);
console.log('Foo.buildName() => ', f.buildName('Fernando'));
