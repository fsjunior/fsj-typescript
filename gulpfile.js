/**
 * Created by fernando.silva on 10/18/16.
 */

var gulp = require('gulp'), 
    ts = require('gulp-typescript');

gulp.task('ts' , () => {
    return gulp.src('src/js/*.ts')
        .pipe(ts({ 
            out :                'index.js',
            target :             'es5' ,
            noImplicitUseStrict: true 
        })) 
        .pipe(gulp.dest('bin'));
});
